/*
    FILE   : covid19_nose_swab.scad
    AUTHORS :
      - Nicolas H.-P. De Coster (Vigon) <ndcoster@meteo.be>
      - Quentin Bolsée <quentinbolsee@hotmail.com>
      - Antonin R <antoninro@mailfence.com>
    DATE   : 2020-03-27
    LICENSE : M.I.T. (https://opensource.org/licenses/MIT)
    NOTES  :
      - 0.3 : size tunings, number of hairs adjusted
      - 0.2 : improved side hairs
      - 0.1 : improved rounded_cube to work with the same parameters as cube()
      - 0.0 : first draft, scraper for testing covid-19 sample.
*/

module swab()
{
total_l = 147;

main_l = 100;
main_w = 3;
main_h = 1;
main_r = main_w/3;

main_break_dist = 15;

break_l = 7;  
break_dist = 1;
break_h = 2.4;
break_r = 1;
break_h_bot = 0.6;

middle_w1 = 1;
middle_w2 = 0.6;
middle_h1 = 1;
middle_h2 = 1.7;

sec_l = 0;
sec_w = 1.75;
sec_h = 0.8;
sec_r = sec_w/3;

third_l = total_l - (sec_l + main_l);
third_w = 1.75;
third_h = sec_h;

scrap_d = 1.8*2; //scraper diameter;
scrap_l = 9;
scrap_h = 0.8;
dig_central = 0.6;
dig_n = 4;
hair_r = 0.8;

//main rod
shift_1 = main_l;
translate([0,-main_w/2,0])
difference() {
rounded_cube([main_l, main_w, main_h], main_r);
translate([main_l-main_break_dist, main_w/2, main_h-(main_h-break_h_bot)/2+0.01])cube([break_dist+2*break_r, main_w, main_h-break_h_bot],center=true);
}

translate([main_l-main_break_dist, 0, 0]){
linear_extrude(middle_h1)
polygon([[-break_dist/2, -main_w/2+break_r],
         [0, -middle_w1/2],
         [break_dist/2, -main_w/2+break_r],
         [break_dist/2, main_w/2-break_r],
         [0, middle_w1/2],
         [-break_dist/2, main_w/2-break_r]]);
translate([0, 0, middle_h2/2])
cube([break_dist, middle_w2, middle_h2], center=true);
}

//breakers
translate([main_l-main_break_dist, 0, 0]){
translate([break_dist/2,-main_w/2,0])
  rounded_cube([break_l, main_w, break_h], break_r);
translate([-break_dist/2-break_l,-main_w/2,0])
  rounded_cube([break_l, main_w, break_h], break_r);
}

//secondary rod
shift_2 = shift_1+sec_l;
translate([shift_1-main_r,-sec_w/2,0])
rounded_cube([sec_l+main_r,sec_w, sec_h],r=sec_r);

//third rod
shift_3 = shift_2+third_l;
translate([shift_2-sec_r,-third_w/2,0])
cube([third_l+sec_r+scrap_l/2, third_w, third_h]);

//scraper
translate([shift_3+scrap_l/2, 0, 0])
scraper(scrap_d, scrap_l, scrap_h, hair_r, dig_central, dig_n);
}

module rounded_cube(size,r=-1,center=false){
  eps = 0.01;
  is_vec = !(size[0] == undef);
  lv = is_vec ? size[0] : size;
  wv = is_vec && len(size)>=2 ? size[1] : lv;
  lshort = min(lv, wv);
  hv = is_vec && len(size)>=3 ? size[2] : lv;
  rv = r==-1 ? lshort/4-eps : min([r, lshort/2-eps]);

  delta = center ? [-lv/2+rv, -wv/2+rv, -hv/2] : [rv, rv, 0];
  
  translate(delta)
  minkowski(){
    cube([lv-2*rv, wv-2*rv, hv/2]);
    cylinder(r=rv, h=hv/2, $fn=50);
  }
}

module scraper(d, l, h, hair_r, central_w, dig_n){
    hair_d = 2*hair_r;
    eps = 0.001;
    
    dig_w = (l-d)/(2*dig_n+1);
    
    translate([0, 0, h])difference(){
    // main shape
    union(){
      rotate([0,90,0])cylinder(d=d, h=l-d, center=true, $fn=100);
      translate([(l-d)/2, 0, 0])sphere(d=d, $fn=70);
      translate([-(l-d)/2, 0, 0])sphere(d=d, $fn=70);
        translate([0, -d/2, -h])rotate([0,90,0])cylinder(d=hair_d, h=l-d, center=true, $fn=100);
        translate([0, d/2, -h])rotate([0,90,0])cylinder(d=hair_d, h=l-d, center=true, $fn=100);
    }
    
    // dig central
    translate([-l/2,-central_w/2,-eps])cube([l,central_w, d/2+2*eps]);
    
    // dig central hairs
    for(i=[0:dig_n]){
      translate([-(l-d)/2+i*2*dig_w,-d/2-hair_r,0])
        cube([dig_w, d+2*hair_r, d/2]);
    }
    
    // dig side hairs
    for(i=[0:dig_n-1]){
      translate([-(l-d)/2+dig_w+i*2*dig_w,-d/2-hair_r,-h-eps])
        cube([dig_w, hair_r, hair_r]);
      translate([-(l-d)/2+dig_w+i*2*dig_w,d/2,-h-eps])
        cube([dig_w, hair_r, hair_r]);
    }

    translate([0, 0, -(d/2+h)])cube([l, d+2*hair_d, d], center=true);
    }

    // bottom
    translate([0, 0, h/2])rounded_cube([l, d, h], r=d/2, center=true);

}

rotate([0,0,90])scale([1.2*0.9455,1,1])swab();
