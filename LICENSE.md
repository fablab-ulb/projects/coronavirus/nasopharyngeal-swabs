The design is inspired by a [John Naskalski's](https://www.thingiverse.com/NaskalskiDesign/about) [creation posted on Thingiverse](https://www.thingiverse.com/thing:4233773) under CC-BY-NC-SA license.

Our model, although similar in spirit, has been completely redesigned on [OpenSCAD](https://www.openscad.org/) by:
  - Nicolas H.-P. De Coster (Vigon) <ndcoster@meteo.be>
  - Quentin Bolsée <quentinbolsee@hotmail.com>
  - Antonin R <antoninro@mailfence.com>

to take into account practitioner's feedback.

Injection model picture credits : Endo Tools Therapeutics.

This project is distributed under [CC BY-NC-SA 3.0](https://creativecommons.org/licenses/by-nc-sa/3.0/) license.
