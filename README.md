# 3D printable swabs for COVID19 testing

**IMPORTANT WARNINGS**
- **no FDA, CE or any other certification: this is only a prototype**
- **to be used only by healthcare professionals. Do not use at home or without the help of a healthcare professional**

# The project

Due the to COVID19 outbreak, a shortage in testing swabs is to be expected in many hospitals and healthcare centers. At the ULB-Erasmus Hospital/Hôpital Erasme, Brussels, we are working of a 3D printable swabs that could be printed rapidly and with cheap hardware/materials.

The whole project is a collaboration of many, including ENT phycicians, FabLabers, Endo Tools Therapeutics and other enthousiasts.

At this stage, the prototype is being tested alongside with standard, commercial swabs on suspected COVID19 patients at the Erasmus Hospital. The results of this study are going to be presented to the public as soon as possible. Preliminary results are encouraging.

At the Belgian level, we expect the need to be approx. 10000/day, so mass production with 3D FDM printers won't be possible. This is why we are also working on a injection-molded version of the model, in partnership with industrials in the area (see below).

# Files

- code/swab.scad : the scad code for a single swab with lateral "hairs"
- files/swab.stl : export of swab.scad to stl, ready to print
- files/40swabs.stl : 40 time swab.stl on a single build plate. Print time approx. 3h17min (27g PLA)
- files/40swabs-Ultimaker.3mf : Cura project file corresponding to 40swabs.stl
- files/adjusted_Naskalski_model.stl : [creation posted on Thingiverse](https://www.thingiverse.com/thing:4233773) but with a slight size change (120% dilation in the longitudinal direction)
- files/50adjusted_Naskalski_model.stl : similar batch print files. Print time approx. 3h26min (30g PLA)
- files/50adjusted_Naskalski_model-Ultimaker.3mf and 50adjusted_Naskalski_model-Prusa.3mf : readymade projects for Ultimaker or Prusa printers respectively

# Prints

All prints have up to now been created with [Ultimaker S3](https://ultimaker.com/3d-printers/ultimaker-s3) printer with 2.85mm Ultimaker filament, 0.2mm layer resolution, AA0.4 head and White Pearl PLA. The build plate is 230 x 190 x 200 mm (9 x 7.4 x 7.9 inches).

The swab.stl is about 177mm long and look like this:

![](images/swab_overview.png)

Here is a closer view of the distal part ("the head"):

![](images/distal_details.png)

Of course many details of the model cannot be reproduced exactly with an FDM printer, see the following picture:

![](images/print_distal.png)

# Injection molded model

As mentionned above, we are also working on injection molded parts in collaboration with Endo Tools Therapeutics. Here are the pictures of the first molded parts using polystyrene:

![](images/injection1.jpg)
![](images/injection2.jpg)

Clinical testing for this prototype is also currently in progress.

# Updates

April 20 2020: results from the field showed that the injection molded model doesn't seem to work very well. We are working to improve the model.

# Authors and License

Please read the [LICENSE.md](./LICENSE.md).
